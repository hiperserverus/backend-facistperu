 'use strict'

 const nodemailer = require('nodemailer');


 process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;

 async function mailVerification(token, correoDestino) {


     /* let testAccount = await nodemailer.createTestAccount();

      let transporter = nodemailer.createTransport({
          host: "smtp.ethereal.email",
          port: 587,
          secure: false, // true for 465, false for other ports
          auth: {
              user: testAccount.user, // generated ethereal user
              pass: testAccount.pass // generated ethereal password
          }
      });*/

     let transporter = nodemailer.createTransport({
         service: 'Gmail',
         auth: {
             user: 'deliveryacistperu@gmail.com',
             pass: 'deliverydelivery'
         }
     });

     // Message object
     let bodyMail = {
         from: '"deliveryAcistPeru 👻" <deliveryacistperu@gmail.com>', // sender address


         to: `twd00027@gmail.com, ${correoDestino}`, // list of receivers

         // Subject of the message
         subject: 'VERIFICACION DE CUENTA DE REGISTRO ✔' + Date.now(),

         // plaintext body
         text: 'HOLA SALUDOS CORDIALES!',

         // HTML body
         html: `<p><b>Hello</b> to myself <img src="cid:note@example.com"/></p>
          pulsa <a href="http://localhost:4200/#/verification/${token}">aquí</a> para activar tu cuenta
            <p>Here's a nyan cat for you as an embedded attachment:<br/><img src="cid:nyan@example.com"/></p>`,

         // AMP4EMAIL
         amp: `<!doctype html>
            <html ⚡4email>
              <head>
                <meta charset="utf-8">
                <style amp4email-boilerplate>body{visibility:hidden}</style>
                <script async src="https://cdn.ampproject.org/v0.js"></script>
                <script async custom-element="amp-anim" src="https://cdn.ampproject.org/v0/amp-anim-0.1.js"></script>
              </head>
              <body>
                <p><b>Hello</b> to myself <amp-img src="https://cldup.com/P0b1bUmEet.png" width="16" height="16"/></p>
              
                <p>No embedded image attachments in AMP, so here's a linked nyan cat instead:<br/>
                  <amp-anim src="https://cldup.com/D72zpdwI-i.gif" width="500" height="350"/></p>
              </body>
            </html>`,

         // An array of attachments
         /*  attachments: [
               // String attachment
               {
                   filename: 'notes.txt',
                   content: 'Some notes about this e-mail',
                   contentType: 'text/plain' // optional, would be detected from the filename
               },

               // Binary Buffer attachment
               {
                   filename: 'image.png',
                   content: Buffer.from(
                       'iVBORw0KGgoAAAANSUhEUgAAABAAAAAQAQMAAAAlPW0iAAAABlBMVEUAAAD/' +
                       '//+l2Z/dAAAAM0lEQVR4nGP4/5/h/1+G/58ZDrAz3D/McH8yw83NDDeNGe4U' +
                       'g9C9zwz3gVLMDA/A6P9/AFGGFyjOXZtQAAAAAElFTkSuQmCC',
                       'base64'
                   ),

                   cid: 'note@example.com' // should be as unique as possible
               }
           ],

           list: {
               // List-Help: <mailto:admin@example.com?subject=help>
               help: 'admin@example.com?subject=help',

               // List-Unsubscribe: <http://example.com> (Comment)
               unsubscribe: [{
                       url: 'http://example.com/unsubscribe',
                       comment: 'A short note about this url'
                   },
                   'unsubscribe@example.com'
               ],

               // List-ID: "comment" <example.com>
               id: {
                   url: 'mylist.example.com',
                   comment: 'This is my awesome list'
               }
           }*/
     };

     await transporter.sendMail(bodyMail, (error, info) => {
         if (error) {
             console.log('Error occurred');
             console.log(error.message);
             return process.exit(1);
         }

         console.log('Message sent successfully!');
         console.log(nodemailer.getTestMessageUrl(info));

         // only needed when using pooled connections
         transporter.close();
     });

 }

 module.exports.mailVerification = mailVerification;

 async function mailRecovery(token, correoDestino) {

    let transporter = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: 'deliveryacistperu@gmail.com',
            pass: 'deliverydelivery'
        }
    });

    // Message object
    let bodyMail = {
        from: '"deliveryAcistPeru 👻" <deliveryacistperu@gmail.com>', // sender address


        to: `twd00027@gmail.com, ${correoDestino}`, // list of receivers

        // Subject of the message
        subject: 'RECUPERACION DE CONTRASEÑA ✔' + Date.now(),

        // plaintext body
        text: 'HOLA SALUDOS CORDIALES!',

        // HTML body
        html: `<p><b>Hello</b> to myself <img src="cid:note@example.com"/></p>
         pulsa <a href="http://localhost:4200/#/verification/${token}">aquí</a> para recperar tu contraseña
           <p>Here's a nyan cat for you as an embedded attachment:<br/><img src="cid:nyan@example.com"/></p>`,

        // AMP4EMAIL
        amp: `<!doctype html>
           <html ⚡4email>
             <head>
               <meta charset="utf-8">
               <style amp4email-boilerplate>body{visibility:hidden}</style>
               <script async src="https://cdn.ampproject.org/v0.js"></script>
               <script async custom-element="amp-anim" src="https://cdn.ampproject.org/v0/amp-anim-0.1.js"></script>
             </head>
             <body>
               <p><b>Hello</b> to myself <amp-img src="https://cldup.com/P0b1bUmEet.png" width="16" height="16"/></p>
             
               <p>No embedded image attachments in AMP, so here's a linked nyan cat instead:<br/>
                 <amp-anim src="https://cldup.com/D72zpdwI-i.gif" width="500" height="350"/></p>
             </body>
           </html>`,

        // An array of attachments
        /*  attachments: [
              // String attachment
              {
                  filename: 'notes.txt',
                  content: 'Some notes about this e-mail',
                  contentType: 'text/plain' // optional, would be detected from the filename
              },

              // Binary Buffer attachment
              {
                  filename: 'image.png',
                  content: Buffer.from(
                      'iVBORw0KGgoAAAANSUhEUgAAABAAAAAQAQMAAAAlPW0iAAAABlBMVEUAAAD/' +
                      '//+l2Z/dAAAAM0lEQVR4nGP4/5/h/1+G/58ZDrAz3D/McH8yw83NDDeNGe4U' +
                      'g9C9zwz3gVLMDA/A6P9/AFGGFyjOXZtQAAAAAElFTkSuQmCC',
                      'base64'
                  ),

                  cid: 'note@example.com' // should be as unique as possible
              }
          ],

          list: {
              // List-Help: <mailto:admin@example.com?subject=help>
              help: 'admin@example.com?subject=help',

              // List-Unsubscribe: <http://example.com> (Comment)
              unsubscribe: [{
                      url: 'http://example.com/unsubscribe',
                      comment: 'A short note about this url'
                  },
                  'unsubscribe@example.com'
              ],

              // List-ID: "comment" <example.com>
              id: {
                  url: 'mylist.example.com',
                  comment: 'This is my awesome list'
              }
          }*/
    };

    await transporter.sendMail(bodyMail, (error, info) => {
        if (error) {
            console.log('Error occurred');
            console.log(error.message);
            return process.exit(1);
        }

        console.log('Message sent successfully!');
        console.log(nodemailer.getTestMessageUrl(info));

        // only needed when using pooled connections
        transporter.close();
    });

}

module.exports.mailRecovery = mailRecovery;