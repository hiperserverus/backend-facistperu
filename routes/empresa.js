const express = require('express');
const app = new express.Router();
const usuario = require('../controllers/empresa.js');

app.route('/empresa').get(usuario.obtenerEmpresas);
app.route('/empresa/:id').get(usuario.buscarEmpresa);
app.route('/empresa/register').post(usuario.registerEmpresa);


module.exports = app;