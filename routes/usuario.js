const express = require('express');
const app = new express.Router();
const usuario = require('../controllers/usuario.js');
var mdAutentication = require('../middlewares/autentication');

app.route('/usuario').get(usuario.obtenerUsuarios);
app.route('/usuario/login').post(usuario.logearUsuario);
app.route('/usuario/register').post(usuario.registerUsuario);
app.route('/usuario/verification').post(mdAutentication.verificaToken, usuario.verificationUser);
app.route('/usuario/activate').put(usuario.activationUser);
app.route('/usuario/recovery').post(usuario.sendRecovery);
//app.route('/usuario/recovery').put(usuario.updatePassword);


module.exports = app;