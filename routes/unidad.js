const express = require('express');
const app = new express.Router();
const unidad = require('../controllers/unidad.js');

app.route('/unidad/register').post(unidad.registerUnidad);


module.exports = app;