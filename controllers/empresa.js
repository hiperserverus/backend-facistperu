const empresas = require('../db_apis/empresa.js');
let usuarios = require('../controllers/usuario.js');

async function obtenerEmpresas(req, res, next) {
    try {

        const rows = await empresas.getEmpresas();

        if (req.params.id) {
            if (rows.length === 1) {
                res.status(200).json(rows[0]);
            } else {
                res.status(404).end();
            }
        } else {
            res.status(200).json(rows);
        }
    } catch (err) {
        next(err);
    }
}
module.exports.obtenerEmpresas = obtenerEmpresas;

async function buscarEmpresa(req, res, next) {

    let id = req.params.id;

    try {

        const rows = await empresas.findPyme(id);

        if (req.params.id) {
            if (rows.length === 1) {
                res.status(200).json(rows[0]);
            } else {
                res.status(404).end();
            }
        } else {

            if (rows.length == 0) {
                res.status(200).json({
                    ok: false,
                    mensaje: "No se encontro la empresa seleccionada"
                });
            }
            res.status(200).json(rows);
        }


    } catch (err) {
        next(err);
    }
}
module.exports.buscarEmpresa = buscarEmpresa;

async function registerEmpresa(req, res, next, connection) {

    let usuarios = require('../controllers/usuario.js');

    let empresa = {};
    let c_direccion_empresa_id_aleatorio = Math.round(usuarios.generaMelo(100000000, 9999999999));


    empresa = {
        c_empresa_id: req.body.empresa_id,
        n_ruc: req.body.ruc,
        c_nomb_cmrcl: req.body.nombre_comercial,
        c_imgn_logo: req.body.imagen,
        c_rzn_scl: req.body.razon_social,
        c_hblt: "N",
        c_drcc_empr_id: c_direccion_empresa_id_aleatorio.toString(),
        c_dominio: req.body.dominio
    }

    console.log(empresa)
    try {


        const rows = await empresas.create(empresa, connection);

        return rows;

        //res.status(200).json(rows);
  


    } catch (err) {
        next(err);
    }
}
module.exports.registerEmpresa = registerEmpresa;