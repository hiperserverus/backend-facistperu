const unidadUsuarios = require('../db_apis/unidadUsuario.js');


async function registerunidadUsuario(req, res, next, connection) {
    const usuarios = require('../controllers/usuario.js');
    let unidadUsuario = {};
    let c_unidad_aleatorio = Math.round(usuarios.generaMelo(100000000, 9999999999));

    unidadUsuario = {
        c_unidad_usuario_id: c_unidad_aleatorio.toString(),
        c_unidad_id: req.body.c_unidad_id,
        c_usuario_id: req.body.c_usuario_id,
        c_asignado: "S",
        c_unidad_usuario_activo: "S",
        c_auditoria_id: null
    }

    try {

        unidadUsuario.catalogo_unidadUsuario = usuarios.catalogo_unidad_usuario;

        const rows = await unidadUsuarios.create(unidadUsuario, connection);

        return rows;
        //res.status(200).json(rows);
      


    } catch (err) {
        next(err);
    }
}
module.exports.registerunidadUsuario = registerunidadUsuario;