const unidades = require('../db_apis/unidad.js');
const oracledb = require('oracledb');


async function registerUnidad(req, res, next, connection) {
    const usuarios = require('../controllers/usuario.js');  
    let unidad = {};
  //  let c_unidad_aleatorio = Math.round(usuarios.generaMelo(100000000, 9999999999));

    unidad = {
        c_unidad_id: "",
        c_unidad_padre_id: null,
        c_tipo_unidad_id: "PA00000002",
        c_cod_unidad: "001",
        c_nom_unidad: req.body.nombre_unidad,
        c_unidad_activa: "N",
        c_auditoria_id: null
    }

    try {
        unidad.catalogo_unidad = usuarios.catalogo_unidad;
       
        const rows = await unidades.create(unidad, connection);

       return rows;
       
       //res.status(200).json(rows);


    } catch (err) {
        next(err);
    }
}
module.exports.registerUnidad = registerUnidad;