const usuarios = require('../db_apis/usuario.js');
const deunidadUsuarios = require('../db_apis/unidadUsuario.js');
const deunidades = require('../db_apis/unidad.js');
const deempresas = require('../db_apis/empresa.js');
const empresas = require('../controllers/empresa.js');
const unidades = require('../controllers/unidad.js');
const unidadUsuarios = require('../controllers/unidadUsuario.js');
const jwt = require('jsonwebtoken');
const database = require('../services/database.js');
const oracledb = require('oracledb');
const delivery = require('../mailer/mail');
let usuarioToken;


const SEED = require('../config/config').SEED;



const catalogo_usuario = "CT00000012";
const catalogo_unidad = "CT00000005";
const catalogo_unidad_usuario = "CT00000013";

let c_unidad_id;
let c_usuario_id;

module.exports = {
    catalogo_usuario,
    catalogo_unidad,
    catalogo_unidad_usuario
}



async function logearUsuario(req, res, next) {

    if (typeof(req.body.type) == "undefined" || typeof(req.body.identificador) == "undefined" || typeof(req.body.clave_usuario) == "undefined") {
        throw new Error (res.status(400).json({ error: "Ingrese datos necesarios" }));
    }   



    let type_identificador = req.body.type;
    let usuario = {};
    let empresa_id;
    let usuario_id;
    let usuario_asignado;
    let usuario_activo;

    usuario = {
        identificador: req.body.identificador,
        c_codigo_usuario: req.body.codigo_usuario,
        c_clave_usuario: req.body.clave_usuario
    }

    if (identificadorType(type_identificador) === 'C_CORREO') {


        await obtenerDominio(usuario.identificador)
            .then(resp => {

                if (resp.length === 0) {

                    throw new Error(res.status(404).json({ error: "No existe usuario para correo indicado" }));

                

                } else {


                    usuario.identificador = resp[0].c_dominio;
                    usuario.c_codigo_usuario = resp[0].c_codigo_usuario;
                    type_identificador = "2";

                }
            }).catch(e => console.log(e));

    }

    if (identificadorType(type_identificador) === 'C_DOMINIO') {


        let identificador_parts = [];

        if (usuario.identificador.indexOf('@') > -1) {

            identificador_parts = usuario.identificador.split("@");
            usuario.identificador = identificador_parts[1];

            if (!usuario.c_codigo_usuario) {
                usuario.c_codigo_usuario = identificador_parts[0];
            }

        }

    }

    try {

        const rows = await usuarios.logUser(usuario);


        if (rows.length == 0) {

            throw new Error(res.status(404).json({ error: "Usuario / Contraseña invalidos" }));

        
        }

        buscarEmpresa(type_identificador, usuario.identificador)
            .then(resp => {


                if (resp.length === 0) {

                    throw new Error(res.status(404).json({ error: "La empresa indicada no existe" }));

                  

                } else {

                    empresa_id = resp[0].c_empresa_id;
                    usuario_id = rows[0].c_usuario_id;

                    buscarUnidad(usuario_id, empresa_id)
                        .then(resp => {

                            if (resp.length === 0) {

                                throw new Error(res.status(404).json({ error: "Usuario no pertenece a la empresa indicada" }));
                               

                            } else {

                                
                                
                                
                                usuario_activo = rows[0].c_usuario_activo;

                                console.log(usuario_activo)

                                if (usuario_activo === "N") {
                                   
                                    throw new Error(res.status(404).json({ error: "Su usuario no se encuentra activo" }));
            
            
                                }

                                usuario_asignado = resp[0].c_asignado;
                                rows[0].c_clave_usuario = ".|."
                                let token = jwt.sign({ usuario: rows[0] }, SEED, { expiresIn: 14400 });
                                rows.push({ token });
                                rows.push({ usuario_asignado });
                                rows.push({ usuario_activo });
                                let menu = obtenerMenu();
                                rows.push({ menu });
                                res.status(200).json(rows);
                            }
                        }).catch(e => console.log(e));
                }
            }).catch(e => console.log(e));
    } catch (err) {
        next(err);
    }
}
module.exports.logearUsuario = logearUsuario;


async function registerUsuario(req, res, next) {

    if ( typeof(req.body.correo) == "undefined" ) {
        throw new Error (res.status(400).json({ error: "Ingrese datos necesarios" }));
    }   

    let usuario = {};
    let nombre_empresa;
    let c_aleatorio = Math.round(generaMelo(100000000, 9999999999));

    usuario = {
        c_usuario_id: c_aleatorio.toString(),
        c_perfil_id: "0",
        c_tipo_usuario_id: "0",
        c_codigo_usuario: req.body.codigo_usuario,
        c_nombre_usuario: req.body.nombre,
        c_apellido_usuario: req.body.apellido,
        c_clave_usuario: req.body.clave_usuario,
        f_asignacion_clave: "",
        c_usuario_bloqueado: "N",
        c_usuario_activo: "N",
        c_dominio: req.body.dominio,
        c_correo: req.body.correo,
        c_telefono: req.body.telefono
    }

    usuarioToken = req.usuario;
    nombre_empresa = req.body.empresa;

    try {

        // GENERACION DEL CODIGO DE USUARIO APARTIR DE LA PRIMERA LETRA DEL NOMBRE Y APELLIDO
        //MAS UN NUMERO ALEATORIO DE 4 DIGITOS

        let connection = await oracledb.getConnection();


        if (!usuario.c_codigo_usuario) {

            let f = usuario.c_nombre_usuario.charAt(0);

            let valido = true;



            for (var i; valido; i++) {

                usuario.c_codigo_usuario = f + usuario.c_apellido_usuario + Math.round(generaMelo(1000, 9999));
                console.log(usuario.c_codigo_usuario);
                console.log(usuario)

                const rows = await usuarios.logUser(usuario);
                console.log(rows)


                if (rows.length == 0) {

                    valido = false;

                    if (typeof(nombre_empresa) == "undefined") {

                        nombre_empresa = usuario.c_nombre_usuario + usuario.c_apellido_usuario;

                    }

                }

            }



            if (!usuario.c_dominio) {

                usuario.c_dominio = usuario.c_codigo_usuario;

            }



            usuario.catalogo_usuario = catalogo_usuario;

            crearUsuario(usuario, connection)
                .then(respuesta => {

                    if (typeof(respuesta) == "undefined") {
               
                        throw new Error(res.status(404).json({ error: "NO SE PUDO CREAR EL USUARIO" }));


                    }

                    console.log({Usuario_Creado: respuesta});



                    c_usuario_id = respuesta.c_usuario_id;

                    let unidad = {
                        body: {
                            nombre_unidad: nombre_empresa,

                        }
                    }


                    unidades.registerUnidad(unidad, res, next, connection)
                        .then(resp => {

                            if (typeof(resp) == "undefined") {
                      
                                throw new Error(res.status(404).json({ error: "NO SE PUDO CREAR LA UNIDAD" }));


                            }

                            console.log({Unidad_Creada: resp});



                            c_unidad_id = resp.c_unidad_id;

                            let empresa = {
                                body: {
                                    empresa_id: c_unidad_id,
                                    ruc: "0",
                                    nombre_comercial: nombre_empresa,
                                    razon_social: "Ninguna",
                                    imagen: ".",
                                    dominio: usuario.c_dominio,
                                }
                            }

                            empresas.registerEmpresa(empresa, res, next, connection)
                                .then(resp => {

                                    if (typeof(resp) == "undefined") {
                                  
                                        throw new Error(res.status(404).json({ error: "NO SE PUDO CREAR LA EMPRESA" }));


                                    }

                                    console.log({Empresa_Creada: resp});



                                    let unidadUsuario = {
                                        body: {
                                            c_unidad_usuario_id: Math.round(generaMelo(100000000, 9999999999)),
                                            c_unidad_id: c_unidad_id,
                                            c_usuario_id: c_usuario_id,
                                            razon_social: "Ninguna ",
                                            imagen: ".",
                                            dominio: usuario.c_dominio,
                                        }
                                    }


                                    unidadUsuarios.registerunidadUsuario(unidadUsuario, res, next, connection)
                                        .then(resp => {

                                           

                                            if (typeof(resp) == "undefined") {
                                             
                                                throw new Error(res.status(404).json({ error: "NO SE PUDO CREAR EL REGISTRO PARA UNIDAD_USUARIO" }));


                                            }
                                            console.log({UnidadEmpresa_Creada: resp});

                                            respuesta.empresa = nombre_empresa;
                                            respuesta.accion = 'Activar';
                                            let tokenVfk = jwt.sign({ usuario: respuesta }, SEED, { expiresIn: 14400 });
                                            //respuesta.usuarioCreador = usuarioToken;
                                            
                                            delivery.mailVerification(tokenVfk, respuesta.c_correo).catch(console.error);
                                            connection.commit();
                                            connection.close();

                                            res.status(200).json(respuesta);



                                        }).catch(e => {
                                            connection.rollback();
                                            connection.close();
                                            console.log(e)
                                            res.status(404).json(e);
                                        });



                                }).catch(e => {
                                    connection.rollback();
                                    connection.close();
                                    console.log(e)
                                    res.status(404).json(e);
                                });



                        }).catch(e => {
                            connection.rollback();
                            connection.close();
                            console.log(e)
                            res.status(404).json(e);
                        });

                }).catch(e => {
                    connection.rollback();
                    connection.close();
                    console.log(e)
                    res.status(404).send(e);
                });




        }






    } catch (err) {
        next(err);
    }



}

module.exports.registerUsuario = registerUsuario;

let usuarioActivation;


async function verificationUser(req, res, next) {

    usuarioActivation = req.usuario;


    return res.status(200).json(usuarioActivation);;
}

module.exports.verificationUser = verificationUser;

async function activationUser(req, res, next) {

    var token = req.body.token;

    jwt.verify(token, SEED, (err, decoded) => {

        if (err) {
            return res.status(401).json({
                ok: false,
                mensaje: 'Token incorrecto',
                errors: err
            });
        }

        req.usuario = decoded.usuario



    });

    console.log(req.usuario)


    let usuarioForActi = {};
    let empresa_id_update;

    usuarioForActi = {

        c_nombre_usuario: req.body.nombre,
        c_apellido_usuario: req.body.apellido,
        c_clave_usuario: req.body.password,
        c_correo: req.body.correo,
        c_dominio: req.body.dominio,
        c_telefono: req.body.telefono,
        c_usuario_activo: "S",
        c_usuario_id: req.body.id
    }


    try {

        let connection = await oracledb.getConnection();

        const rows = await usuarios.activateUser(usuarioForActi, connection);

        if (rows == null) {

          throw new Error(res.status(404).json({ error: "EL Usuario que intenta activar no existe en la Base de datos" }));
        }

        if (req.usuario.empresa != req.body.empresa || req.usuario.c_dominio != req.body.dominio) {

            console.log('La empresa cambio');

            deunidadUsuarios.findUnidadUsuario(usuarioForActi.c_usuario_id, connection)
                .then(resp => {

                    if (typeof(resp) == "undefined") {

                        throw new Error(res.status(404).json({ error: "NO ENCONTRO LA EMPRES AASIGNADA A ESTE USUARIO" }));


                    }

                    empresa_id_update = resp[0].c_unidad_id;


                    let unidad = {

                        c_nom_unidad: req.body.empresa,
                        c_empresa_id: empresa_id_update

                    }

                    deunidades.updateUnidad(unidad, connection)
                        .then(resp => {

                            if (typeof(resp) != "undefined") {

                                console.log('Nombre unidad actualizada: ');


                            }

                            let empresa = {

                                c_empresa_id: empresa_id_update,
                                c_nomb_cmrcl: req.body.empresa,
                                c_dominio: usuarioForActi.c_dominio

                            }

                                deempresas.updatePyme(empresa, connection)
                                .then(resp => {

                                    if (typeof(resp) != "undefined") {

                                        console.log('Nombre empresa actualizada: ');



                                    }

                                    console.log('Actualizacion completa');
                                    res.status(200).json(rows);
                                    connection.commit();
                                    connection.close();




                                }).catch(e => {
                                    console.log(e);
                                    connection.rollback();
                                })


                        }).catch(e => {
                            console.log(e);
                            connection.rollback();
                        })




                }).catch(e => {
                    console.log(e);
                    connection.rollback();
                });





        } else {

            console.log('Actualizacion completa');
            res.status(200).json(rows);
            connection.commit();
            connection.close();
        }






    } catch (err) {
        next(err);
    }



}

module.exports.activationUser = activationUser;


async function sendRecovery(req, res, next) {

    let c_correo;

    if (typeof(req.body.correo) == "undefined" ) {
        throw new Error(res.status(404).json({ error: "Porfavor ingrese un correo electronico" }));
    }

try {

    

    c_correo = req.body.correo;

    console.log(req.body)

    usuarios.findDom(c_correo)
            .then( resp => {

               

                if (typeof(resp) == "undefined" || resp.length == 0) {

                    throw new Error(res.status(404).json({ error: "No se encontro el correo subministrado" }));


                } 

            

                const usr = Object.assign({}, resp[0]);
                usr.accion = 'Actualizar';
                

                let tokenRcv = jwt.sign({ usuario: usr }, SEED, { expiresIn: 14400 });

                delivery.mailRecovery(tokenRcv, usr.c_correo).catch(console.error);

                res.status(200).json({mensaje: `Correo de restablecimiento enviado exitosamente a : ${c_correo}`})


            }).catch(e => console.log(e))


} catch(e){
    next(err);
}

}

module.exports.sendRecovery = sendRecovery;

async function obtenerUsuarios(req, res, next) {
    try {

        const rows = await usuarios.getUsuarios();

        if (rows.length === 0) {

            return res.status(200).json({
                ok: false,
                mensaje: "No se encontraron usuarios en la Base de datos"
            });

        }

        res.status(200).json(rows);

    } catch (err) {
        next(err);
    }
}
module.exports.obtenerUsuarios = obtenerUsuarios;


async function actualizarEmpresa(empresa) {


    const rows = await usuarios.updatePyme(empresa);

    return rows

}

async function buscarEmpresa(type_identificador, valor) {


    let columna = identificadorType(type_identificador);


    const rows = await usuarios.findPyme(columna, valor);

    return rows

}

async function buscarUnidad(usuario_id, empresa_id) {


    const rows = await usuarios.findUnidad(usuario_id, empresa_id);



    return rows

}

async function obtenerDominio(identificador) {


    const rows = await usuarios.findDom(identificador);



    return rows

}

function obtenerMenu() {


    let menu = [{
            titulo: 'Principal',
            icono: 'mdi mdi-gauge',
            submenu: [
                { titulo: 'Dashboard', url: '/dashboard' },
                { titulo: 'ProgressBar', url: '/dashboard' },
                { titulo: 'Gráficas', url: '/dashboard' },
                { titulo: 'Promesas', url: '/dashboard' },
                { titulo: 'RxJs', url: '/rxjs' }
            ]
        },
        {
            titulo: 'Mantenimientos',
            icono: 'mdi mdi-folder-lock-open',
            submenu: [
                { titulo: 'Usuarios', url: '/usuarios' }

            ]
        }
    ];

    return menu;
}

function identificadorType(type_identificador) {
    let columna;
    switch (type_identificador) {
        case '1':
            columna = "N_RUC";
            break;

        case '2':
            columna = "C_DOMINIO";
            break;

        case '3':
            columna = "C_CORREO";
            break;

        default:
            columna = "N_RUC";
    }

    return columna;
}

function generaMelo(min, max) {
    return Math.random() * (max - min) + min;
}

module.exports.generaMelo = generaMelo;

async function crearUsuario(usuario, connection) {


    const rows = await usuarios.create(usuario, connection);



    return rows

}