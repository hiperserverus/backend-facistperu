const oracledb = require('oracledb');
const database = require('../services/database.js');
const empresas = require('../db_apis/empresa.js');
const unidades = require('../db_apis/unidadUsuario.js');

const optionsQuery = " ORDER BY " + `Seguridad.TB_SG_USUARIO.c_usuario_id ` + " ASC ";

const baseQuery =
    `SELECT Seguridad.TB_SG_USUARIO.c_usuario_id "c_usuario_id",
    Seguridad.TB_SG_USUARIO.c_perfil_id "c_perfil_id",
    Seguridad.TB_SG_USUARIO.c_tipo_usuario_id "c_tipo_usuario_id", 
    Seguridad.TB_SG_USUARIO.c_codigo_usuario "c_codigo_usuario",
    Seguridad.TB_SG_USUARIO.c_nombre_usuario "c_nombre_usuario",
    Seguridad.TB_SG_USUARIO.c_apellido_usuario "c_apellido_usuario" , 
    Seguridad.TB_SG_USUARIO.c_clave_usuario "c_clave_usuario" , 
    Seguridad.TB_SG_USUARIO.f_asignacion_clave "f_asignacion_clave" , 
    Seguridad.TB_SG_USUARIO.c_usuario_bloqueado "c_usuario_bloqueado" ,   
    Seguridad.TB_SG_USUARIO.c_usuario_activo "c_usuario_activo",
    Seguridad.TB_SG_USUARIO.C_AUDITORIA_ID "c_auditoria_id",
    Seguridad.TB_SG_USUARIO.C_DOMINIO "c_dominio",
    Seguridad.TB_SG_USUARIO.C_CORREO "c_correo",
    Seguridad.TB_SG_USUARIO.C_TELEFONO "c_telefono"
     FROM Seguridad.TB_SG_USUARIO 
    `

const createSql = " INSERT INTO " +
    `Seguridad.TB_SG_USUARIO (
    Seguridad.TB_SG_USUARIO.c_usuario_id ,
    Seguridad.TB_SG_USUARIO.c_perfil_id,
    Seguridad.TB_SG_USUARIO.c_tipo_usuario_id,
    Seguridad.TB_SG_USUARIO.c_codigo_usuario,
    Seguridad.TB_SG_USUARIO.c_nombre_usuario,
    Seguridad.TB_SG_USUARIO.c_apellido_usuario,
    Seguridad.TB_SG_USUARIO.c_clave_usuario,
    Seguridad.TB_SG_USUARIO.f_asignacion_clave,
    Seguridad.TB_SG_USUARIO.c_usuario_bloqueado,
    Seguridad.TB_SG_USUARIO.c_usuario_activo,
    Seguridad.TB_SG_USUARIO.C_DOMINIO,
    Seguridad.TB_SG_USUARIO.C_CORREO,
    Seguridad.TB_SG_USUARIO.C_TELEFONO
  ) ` + " VALUES " + ` (
    :c_usuario_id,
    :c_perfil_id,
    :c_tipo_usuario_id,
    :c_codigo_usuario,
    :c_nombre_usuario,
    :c_apellido_usuario,
    :c_clave_usuario,
    :f_asignacion_clave,
    :c_usuario_bloqueado,
    :c_usuario_activo,
    :c_dominio,
    :c_correo,
    :c_telefono
  ) `;

const updateSql =
    `update Seguridad.TB_SG_USUARIO ` +
    "set " +
    ` 
    Seguridad.TB_SG_USUARIO.c_nombre_usuario = :c_nombre_usuario,
    Seguridad.TB_SG_USUARIO.c_apellido_usuario = :c_apellido_usuario,
    Seguridad.TB_SG_USUARIO.c_clave_usuario = :c_clave_usuario,
    Seguridad.TB_SG_USUARIO.C_CORREO = :c_correo,
    Seguridad.TB_SG_USUARIO.C_DOMINIO = :c_dominio,
    Seguridad.TB_SG_USUARIO.C_TELEFONO = :c_telefono,
    Seguridad.TB_SG_USUARIO.c_usuario_activo = :c_usuario_activo ` + "where" + ` Seguridad.TB_SG_USUARIO.c_usuario_id = :c_usuario_id`;

let opts = {
    outFormat: oracledb.OBJECT,
    autoCommit: false
}
module.exports.opts = opts;

let opts2 = {
    outFormat: oracledb.OBJECT,
    autoCommit: true
}

module.exports.opts2 = opts2;

async function getUsuarios() {

    let query = baseQuery + optionsQuery;

    const result = await database.simpleExecute(query);

    return result.rows;
}

module.exports.getUsuarios = getUsuarios;


async function logUser(usuario) {
    let query = baseQuery;
    const binds = {};

    if (usuario.c_codigo_usuario) {
        binds.codigo_usuario = usuario.c_codigo_usuario;


        query += `\nwhere Seguridad.TB_SG_USUARIO.c_codigo_usuario = :codigo_usuario `;
    }

    if (usuario.c_clave_usuario) {

        binds.clave_usuario = usuario.c_clave_usuario;

        query += `\nand Seguridad.TB_SG_USUARIO.c_clave_usuario = :clave_usuario`;
    }

    const result = await database.simpleExecute(query, binds);

    return result.rows;
}

module.exports.logUser = logUser;

async function findDom(correo) {
    let query = baseQuery;
    const binds = {};

    if (correo) {
        binds.C_CORREO = correo

        query += `\nwhere Seguridad.TB_SG_USUARIO.C_CORREO = :C_CORREO`;
    }

    const result = await database.simpleExecute(query, binds);

    return result.rows;
}

module.exports.findDom = findDom;

async function findPyme(columna, valor) {
    let query = empresas.baseQuery;
    const binds = {};

    if (valor) {
        binds.valor = valor;

        query += `\nwhere BASE.TB_MG_EMPRESA.` + columna + ` = :valor `;

    }


    const result = await database.simpleExecute(query, binds);

    return result.rows;
}

module.exports.findPyme = findPyme;

async function findUnidad(usuario_id, empresa_id) {
    let query = unidades.baseQuery;
    const binds = {};

    if (empresa_id) {
        binds.C_USUARIO_ID = usuario_id;
        binds.C_UNIDAD_ID = empresa_id;

        query += `\nwhere Seguridad.TB_SG_UNIDAD_USUARIO.C_USUARIO_ID  = :C_USUARIO_ID and Seguridad.TB_SG_UNIDAD_USUARIO.C_UNIDAD_ID = :C_UNIDAD_ID`;

    }


    const result = await database.simpleExecute(query, binds);

    return result.rows;
}

module.exports.findUnidad = findUnidad;



async function create(usuario, connection) {

    const nuevoUsuario = Object.assign({}, usuario);
    delete nuevoUsuario.catalogo_usuario;


    let corr = {

        catalogo: usuario.catalogo_usuario,
        unidad: '',
        correlativo: { type: oracledb.STRING, dir: oracledb.BIND_OUT, maxSize: 10 },
        retval: { dir: oracledb.BIND_OUT, type: oracledb.NUMBER }
    }



    let result = await database.ejecutaMelo(
        `BEGIN 
       :retval :=  BA.PA_CRUD_CRRLTVO_CTLGO_SEL(:catalogo, :unidad, :correlativo); 
      END;`, corr, opts, connection
    );

    nuevoUsuario.c_usuario_id = result.outBinds.correlativo;



    result = await database.ejecutaMelo(createSql, nuevoUsuario, opts, connection);


    return nuevoUsuario;
}

module.exports.create = create;

async function activateUser(usuario, connection) {

    const usuarioActualizar = Object.assign({}, usuario);


    const result = await database.ejecutaMelo(updateSql, usuarioActualizar, opts, connection);


    if (result.rowsAffected && result.rowsAffected === 1) {
        return usuarioActualizar;
    } else {
        return null;
    }
}

module.exports.activateUser = activateUser;