const oracledb = require('oracledb');
const database = require('../services/database.js');
const usuarios = require('../db_apis/usuario.js');

const createSql = " INSERT INTO " +
    `BASE.TB_MG_UNIDAD (
    BASE.TB_MG_UNIDAD.C_UNIDAD_ID,
    BASE.TB_MG_UNIDAD.C_TIPO_UNIDAD_ID,
    BASE.TB_MG_UNIDAD.C_COD_UNIDAD,
    BASE.TB_MG_UNIDAD.C_NOM_UNIDAD,
    BASE.TB_MG_UNIDAD.C_UNIDAD_ACTIVA,
    BASE.TB_MG_UNIDAD.C_AUDITORIA_ID
  ) ` + " VALUES " + ` (
    :c_unidad_id,
    :c_tipo_unidad_id,
    :c_cod_unidad,
    :c_nom_unidad,
    :c_unidad_activa,
    :c_auditoria_id
  ) `;

module.exports.createSql = createSql;

const updateSql =
    `update BASE.TB_MG_UNIDAD ` +
    "set " +
    ` 
    BASE.TB_MG_UNIDAD.C_NOM_UNIDAD = :c_nom_unidad ` + "where" + ` BASE.TB_MG_UNIDAD.C_UNIDAD_ID = :c_empresa_id`;

module.exports.updateSql = updateSql;

async function create(unidad, connection) {

    const nuevaUnidad = Object.assign({}, unidad);
    delete nuevaUnidad.catalogo_unidad;
    delete nuevaUnidad.c_unidad_padre_id;

    let corr = {

        catalogo: unidad.catalogo_unidad,
        unidad: '',
        correlativo: { type: oracledb.STRING, dir: oracledb.BIND_OUT, maxSize: 10 },
        retval: { dir: oracledb.BIND_OUT, type: oracledb.NUMBER }
    }





    let result = await database.ejecutaMelo(
        `BEGIN 
   :retval :=  BA.PA_CRUD_CRRLTVO_CTLGO_SEL(:catalogo, :unidad, :correlativo); 
  END;`, corr, usuarios.opts, connection
    );

    nuevaUnidad.c_unidad_id = result.outBinds.correlativo;

    result = await database.ejecutaMelo(createSql, nuevaUnidad, usuarios.opts, connection);



    return nuevaUnidad;
}

module.exports.create = create;

async function updateUnidad(unidad, connection) {

    const unidadUpdated = Object.assign({}, unidad);

    console.log(unidadUpdated)

    const result = await database.ejecutaMelo(updateSql, unidadUpdated, usuarios.opts, connection);


    if (result.rowsAffected && result.rowsAffected === 1) {
        return unidadUpdated;
    } else {
        return null;
    }
}

module.exports.updateUnidad = updateUnidad;