const oracledb = require('oracledb');
const database = require('../services/database.js');
const usuarios = require('../db_apis/usuario.js');

const optionsQuery = " ORDER BY " + `BASE.TB_MG_EMPRESA.C_EMPRESA_ID ` + " ASC ";

const baseQuery =
    `SELECT BASE.TB_MG_EMPRESA.C_EMPRESA_ID "c_empresa_id",
    BASE.TB_MG_EMPRESA.C_EMPRESA_ID "c_perfil_id",
    BASE.TB_MG_EMPRESA.N_RUC "n_ruc", 
    BASE.TB_MG_EMPRESA.C_NOMB_CMRCL "c_nomb_cmrcl",
    BASE.TB_MG_EMPRESA.C_RZN_SCL "c_rzn_scl",
    BASE.TB_MG_EMPRESA.C_IMGN_LOGO "c_imgn_logo" , 
    BASE.TB_MG_EMPRESA.C_HBLT "c_hblt" , 
    BASE.TB_MG_EMPRESA.C_DRCC_EMPR_ID "c_drcc_empr_id" , 
    BASE.TB_MG_EMPRESA.C_DOMINIO "c_dominio" FROM BASE.TB_MG_EMPRESA
    `
module.exports.baseQuery = baseQuery;


const createSql = " INSERT INTO " +
    `BASE.TB_MG_EMPRESA (
    BASE.TB_MG_EMPRESA.C_EMPRESA_ID ,
    BASE.TB_MG_EMPRESA.N_RUC,
    BASE.TB_MG_EMPRESA.C_NOMB_CMRCL,
    BASE.TB_MG_EMPRESA.C_RZN_SCL,
    BASE.TB_MG_EMPRESA.C_IMGN_LOGO,
    BASE.TB_MG_EMPRESA.C_HBLT,
    BASE.TB_MG_EMPRESA.C_DRCC_EMPR_ID,
    BASE.TB_MG_EMPRESA.C_DOMINIO
  ) ` + " VALUES " + ` (
    :c_empresa_id,
    :n_ruc,
    :c_nomb_cmrcl,
    :c_rzn_scl,
    :c_imgn_logo,
    :c_hblt,
    :c_drcc_empr_id,
    :c_dominio
  ) `;

module.exports.createSql = createSql;

const updateSql =
    `update BASE.TB_MG_EMPRESA ` +
    "set " +
    ` 
    BASE.TB_MG_EMPRESA.C_NOMB_CMRCL = :c_nomb_cmrcl,
    BASE.TB_MG_EMPRESA.C_DOMINIO = :c_dominio ` + "where" + ` BASE.TB_MG_EMPRESA.C_EMPRESA_ID = :c_empresa_id`;

module.exports.updateSql = updateSql;

async function getEmpresas() {

    let query = baseQuery + optionsQuery;

    const result = await database.simpleExecute(query);

    return result.rows;
}

module.exports.getEmpresas = getEmpresas;

async function findPyme(id) {
    let query = baseQuery;
    const binds = {};

    if (id) {
        binds.C_EMPRESA_ID = id;

        query += `\nwhere BASE.TB_MG_EMPRESA.C_EMPRESA_ID = :C_EMPRESA_ID `;
    }

    const result = await database.simpleExecute(query, binds);

    return result.rows;
}

module.exports.findPyme = findPyme;

async function create(empresa, connection) {

    const nuevaEmpresa = Object.assign({}, empresa);


    result = await database.ejecutaMelo(createSql, nuevaEmpresa, usuarios.opts, connection);


    return nuevaEmpresa;
}

module.exports.create = create;

async function updatePyme(empresa, connection) {

    const empresaActualizar = Object.assign({}, empresa);

    console.log(empresaActualizar)

    const result = await database.ejecutaMelo(updateSql, empresaActualizar, usuarios.opts, connection);


    if (result.rowsAffected && result.rowsAffected === 1) {
        return empresaActualizar;
    } else {
        return null;
    }
}

module.exports.updatePyme = updatePyme;