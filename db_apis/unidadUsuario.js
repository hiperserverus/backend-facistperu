const oracledb = require('oracledb');
const database = require('../services/database.js');
const usuarios = require('../db_apis/usuario.js');

const optionsQuery = " ORDER BY " + `Seguridad.TB_SG_UNIDAD_USUARIO.C_UNIDAD_USUARIO_ID ` + " ASC ";

const baseQuery =
    `SELECT Seguridad.TB_SG_UNIDAD_USUARIO.C_UNIDAD_USUARIO_ID "c_unidad_usuario_id",
    Seguridad.TB_SG_UNIDAD_USUARIO.C_UNIDAD_ID "c_unidad_id",
    Seguridad.TB_SG_UNIDAD_USUARIO.C_USUARIO_ID "c_usuario_id", 
    Seguridad.TB_SG_UNIDAD_USUARIO.C_ASIGNADO "c_asignado",
    Seguridad.TB_SG_UNIDAD_USUARIO.C_UNIDAD_USUARIO_ACTIVO "c_unidad_usuario_activo",
    Seguridad.TB_SG_UNIDAD_USUARIO.C_AUDITORIA_ID "c_auditoria_id" FROM Seguridad.TB_SG_UNIDAD_USUARIO
    `
module.exports.baseQuery = baseQuery;

const createSql = " INSERT INTO " +
    `SEGURIDAD.TB_SG_UNIDAD_USUARIO (
        SEGURIDAD.TB_SG_UNIDAD_USUARIO.C_UNIDAD_USUARIO_ID,
        SEGURIDAD.TB_SG_UNIDAD_USUARIO.C_UNIDAD_ID,
        SEGURIDAD.TB_SG_UNIDAD_USUARIO.C_USUARIO_ID,
        SEGURIDAD.TB_SG_UNIDAD_USUARIO.C_ASIGNADO,
        SEGURIDAD.TB_SG_UNIDAD_USUARIO.C_UNIDAD_USUARIO_ACTIVO,
        SEGURIDAD.TB_SG_UNIDAD_USUARIO.C_AUDITORIA_ID
  ) ` + " VALUES " + ` (
    :c_unidad_usuario_id,
    :c_unidad_id,
    :c_usuario_id,
    :c_asignado,
    :c_unidad_usuario_activo,
    :c_auditoria_id
  ) `;

module.exports.createSql = createSql;

async function getUnidades() {

    let query = baseQuery + optionsQuery;

    const result = await database.simpleExecute(query);

    return result.rows;
}

module.exports.getUnidades = getUnidades;

async function create(unidadUsuario, connection) {

    const nuevaunidadUsuario = Object.assign({}, unidadUsuario);
    delete nuevaunidadUsuario.catalogo_unidadUsuario;

    let corr = {

        catalogo: unidadUsuario.catalogo_unidadUsuario,
        unidad: '',
        correlativo: { type: oracledb.STRING, dir: oracledb.BIND_OUT, maxSize: 10 },
        retval: { dir: oracledb.BIND_OUT, type: oracledb.NUMBER }
    }



    let result = await database.ejecutaMelo(
        `BEGIN 
   :retval :=  BA.PA_CRUD_CRRLTVO_CTLGO_SEL(:catalogo, :unidad, :correlativo); 
  END;`, corr, usuarios.opts, connection
    );

    nuevaunidadUsuario.c_unidad_usuario_id = result.outBinds.correlativo;

    result = await database.ejecutaMelo(createSql, nuevaunidadUsuario, usuarios.opts, connection);


    return nuevaunidadUsuario;
}

module.exports.create = create;

async function findUnidadUsuario(id_usuario, connection) {
    let query = baseQuery;
    const binds = {};

    if (id_usuario) {
        binds.C_USUARIO_ID = id_usuario;

        query += `\nwhere SEGURIDAD.TB_SG_UNIDAD_USUARIO.C_USUARIO_ID = :C_USUARIO_ID `;
    }

    const result = await database.ejecutaMelo(query, binds, usuarios.opts, connection);

    return result.rows;
}

module.exports.findUnidadUsuario = findUnidadUsuario;