const http = require('http');
const express = require('express');
const morgan = require('morgan');
const webServerConfig = require('../config/web-server.js');

const usuarioRoutes = require('../routes/usuario.js');
const empresaRoutes = require('../routes/empresa.js');
const unidadRoutes = require('../routes/unidad.js');
const unidadusuarioRoutes = require('../routes/unidadUsuario.js');

var bodyParser = require('body-parser');

let httpServer;

function initialize() {
    return new Promise((resolve, reject) => {
        const app = express();
        httpServer = http.createServer(app);

        // Combines logging info from request and response
        app.use(morgan('combined'));

        // Parse incoming JSON requests and revive JSON.
        app.use(express.json({
            reviver: reviveJson
        }));

        // body parser
        // parse application/x-www-form-urlencoded
        app.use(bodyParser.urlencoded({ extended: false }));
        app.use(bodyParser.json());

        app.use(function(req, res, next) {
            res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, XMLHttpRequest");
            res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
            next();
        });

        // Mount the router at /api so all its routes start with /api

        app.use('/api', usuarioRoutes);
        app.use('/api', empresaRoutes);
        app.use('/api', unidadRoutes);
        app.use('/api', unidadusuarioRoutes);

        httpServer.listen(webServerConfig.port)
            .on('listening', () => {
                console.log(`Web server listening on localhost:${webServerConfig.port}`);

                resolve();
            })
            .on('error', err => {
                reject(err);
            });
    });
}

module.exports.initialize = initialize;

function close() {
    return new Promise((resolve, reject) => {
        httpServer.close((err) => {
            if (err) {
                reject(err);
                return;
            }

            resolve();
        });
    });
}

module.exports.close = close;

const iso8601RegExp = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(\.\d{3})?Z$/;

function reviveJson(key, value) {
    // revive ISO 8601 date strings to instances of Date
    if (typeof value === 'string' && iso8601RegExp.test(value)) {
        return new Date(value);
    } else {
        return value;
    }
}